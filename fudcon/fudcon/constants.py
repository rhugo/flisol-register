# -*- coding: utf-8 -*-
import os

SELF_DIR = os.path.dirname(os.path.realpath(__file__))
STATIC_DIR = '%s/../static/' % SELF_DIR